
FROM nginx

LABEL version="2.0.0" maintainer="Welisson Hudson Fernandes Lima"

COPY ./site /usr/share/nginx/html/

EXPOSE 80

WORKDIR /usr/share/nginx/html/

ENTRYPOINT ["/usr/sbin/nginx"]

CMD ["-g", "daemon off;"]
